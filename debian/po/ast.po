# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: phpmyadmin@packages.debian.org\n"
"POT-Creation-Date: 2019-10-22 22:04+0200\n"
"PO-Revision-Date: 2017-07-06 01:52+0000\n"
"Last-Translator: Xuacu Saturio <xuacusk8@gmail.com>\n"
"Language-Team: Asturian <https://hosted.weblate.org/projects/phpmyadmin/"
"debian/ast/>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.16-dev\n"

#. Type: multiselect
#. Description
#: ../templates:2001
msgid "Web server to reconfigure automatically:"
msgstr "Sirvidor web a reconfigurar automáticamente:"

#. Type: multiselect
#. Description
#: ../templates:2001
msgid ""
"Please choose the web server that should be automatically configured to run "
"phpMyAdmin."
msgstr ""
"Escueyi'l sirvidor web que tendría de configurase automáticamente pa "
"executar phpMyAdmin."

#~ msgid "Username for web-based setup system:"
#~ msgstr "Nome d'usuariu pal sistema de configuración basáu en web:"

#~ msgid ""
#~ "The setup system for phpMyAdmin may be used, after installation, from "
#~ "http://localhost/phpmyadmin/setup/index.php."
#~ msgstr ""
#~ "Después de la instalación, puede usase'l sistema de configuración de "
#~ "phpMyAdmin dende http://localhost/phpmyadmin/setup/index.php."

#~ msgid ""
#~ "Access to this system requires identification with a username and a "
#~ "password."
#~ msgstr ""
#~ "L'accesu a esti sistema rique identificarse con un nome d'usuariu y una "
#~ "contraseña."

#~ msgid ""
#~ "If you leave this field empty, the default username ('admin') will be "
#~ "used."
#~ msgstr ""
#~ "Si dexes esti campu en blancu, utilizaráse'l nome d'usuariu predetermináu "
#~ "(«admin»)."

#~ msgid "Password for web-based setup system:"
#~ msgstr "Contraseña pal sistema de configuración basáu en web:"

#~ msgid ""
#~ "Usernames and passwords may be managed with the `htpasswd' command and "
#~ "are stored in /etc/phpmyadmin/htpasswd.setup."
#~ msgstr ""
#~ "Los nomes d'usuariu y les contraseñes pueden alministrase col comandu "
#~ "«htpasswd» y se guarden en «/etc/phpmyadmin/htpasswd.setup»."

#~ msgid ""
#~ "If you leave this field empty, access to the web-based setup will be "
#~ "disabled."
#~ msgstr ""
#~ "Si dexes esti campu en blancu,  l'accesu al sistema de configuración "
#~ "basáu en web desactivaráse."
